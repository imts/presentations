+++
title = "Retour sur les rapports"
description = "Quelques conseils pour la rédaction de vos rapports"
outputs = ["Reveal"]
[logo]
src = "logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

<br />
#### 🎓 Quelques conseils pour la rédaction de vos rapports 🔖
<br />
Un premier retour : 

- Sur la forme 🔍
- Sur le contenu 🩺
- Sur les éléments du rapport
- Sur le reste aussi ...

---

### 📝 La rédaction

<ul>
{{% fragment %}}<li> 🧮 Avant tout un dossier technique</li>{{% /fragment %}} 
{{% fragment %}}<li>  ⏰ Au présent</li>{{% /fragment %}} 
{{% fragment %}}<li> 🔪 Faire des phrases courtes</li>{{% /fragment %}} 
{{% fragment %}}<li> 👉 Utiliser les listes à puces</li>{{% /fragment %}} 
</ul>

---

## 🔎 Les éléments du rapport 🔍


---

### 🕌 La page de garde 

<ul>
{{% fragment %}}<li> 🧮 première impression sur le rapport</li>{{% /fragment %}} 
{{% fragment %}}<li>faire simple, beau et complet </li>{{% /fragment %}} 
{{% fragment %}}<li>L’idéal est 1 seule page de garde</li>{{% /fragment %}} 
</ul>

--- 
### Le sommaire

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>⚡ Généré automatiquement, sous peine d'être incorrect </li>{{% /fragment %}} 
{{% fragment %}}<li>Liste numérotée des parties du rapport</li>{{% /fragment %}} 
{{% fragment %}}<li>🔗 Lien vers les numéros de page</li>{{% /fragment %}}
{{% fragment %}}<li>🎰 Numérotation imbriquée : 1., 1.1, 1.1.1, 1.1.1.1, … </li>{{% /fragment %}}
{{% fragment %}}<li>🦥 Permet de se situer facilement dans le rapport</li>{{% /fragment %}}
</ul>

---

### L'introduction 

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>🤝 Présente le contexte du stage</li>{{% /fragment %}} 
{{% fragment %}}<li>🔦 Les thèmes abordés dans chaque partie</li>{{% /fragment %}} 
{{% fragment %}}<li>🙈 Pas la peine de dévoiler le contenu des parties</li>{{% /fragment %}} 
</ul>

---

###  Le résumé : un Nini

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>👥 Pitch de base pour expliquer votre travail</li>{{% /fragment %}}
{{% fragment %}}<li>⏳Ça doit prendre ~1 minute à lire</li>{{% /fragment %}}
{{% fragment %}}<li>Ni un sommaire </li>{{% /fragment %}}
{{% fragment %}}<li> Ni une introduction </li>{{% /fragment %}}
</ul>
{{% fragment %}}

Il présente :
  - le sujet du travail en 1 phrase, 
  - puis le contexte (où et comment a été réalisé le stage, seul ou à plusieurs, suivi par qui)
  - les réalisations majeures (principaux résultats, site, documentation, ...) 
  - les langages et principaux outils pour réaliser le stage. 

{{% /fragment %}}

---

### Les **annexes** 

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>Les choses moins importantes pour la compréhension</li>{{% /fragment %}}
{{% fragment %}}<li>Plus d'explications pour un lecteur intéressé</li>{{% /fragment %}}
{{% fragment %}}<li>Donner un titre et au moins une ligne de description de ce qu'elle contient</li>{{% /fragment %}}
{{% fragment %}}<li>Ce n'est pas une partie où l'on met tout ce qui ne tient pas dans le rapport</li>{{% /fragment %}}
</ul>

---

### **Ressources / bibliographie** 

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>Liste des documents utilisés</li>{{% /fragment %}}
{{% fragment %}}<li>Ressources en ligne, indiquer </li>{{% /fragment %}}
{{% fragment %}}<ul><li>les titres des pages</li><li>lien vers les sites</li>
<li>l' auteur ou l'éditeur</li></ul>{{% /fragment %}}
{{% fragment %}}<li>Montrer les sites en _anglais_</li>{{% /fragment %}}
{{% fragment %}}<li>Un item par référence</li>{{% /fragment %}}
</ul>

---

### **Livrables du stage** 

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>Section à ajouter avant la conclusion</li>{{% /fragment %}}
{{% fragment %}}<li>Lien vers le site réalisé</li>{{% /fragment %}}
{{% fragment %}}<li>Documentation utilisateur</li>{{% /fragment %}}
{{% fragment %}}<li>Documentation et code sources du projet</li>{{% /fragment %}}
{{% fragment %}}<li>Comment est envisagée la maintenance</li>{{% /fragment %}} 
</ul>

---

### Règles typographiques

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>Pas de ponctuation à la fin d'un titre</li>{{% /fragment %}}
{{% fragment %}}<li>Ex: pas de ':' ou de '.' à la fin d'un titre</li>{{% /fragment %}}
{{% fragment %}}<li>Éventuellement '?' ou '!'</li>{{% /fragment %}}
{{% fragment %}}<li>Entre parenthèses = chose peu importante</li>{{% /fragment %}}
</ul>


---

### Inclusion de lignes de code

```javascript
function toto(paramz) {
  console.log(paramz);
}
```

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>Isolez les lignes</li>{{% /fragment %}}
{{% fragment %}}<li>🎨 Coloration syntaxique</li>{{% /fragment %}}
{{% fragment %}}<li>📋 Faire un copier-coller depuis visual studio</li>{{% /fragment %}}
{{% fragment %}}<li>Permet au lecteur de tester vos bouts de code</li>{{% /fragment %}}
</ul>


--- 

### Remarques diverses

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>Git ne signifie pas CI / CD par défaut</li>{{% /fragment %}}
{{% fragment %}}<li>Intégration continue (CI) : automatise les tests de non régression sur le code à chaque commit</li>{{% /fragment %}}
{{% fragment %}}<li>Déploiement continu (CD) : code est mis en production à chaque commit</li>{{% /fragment %}}
</ul>

---

## Orthographe 

{{% fragment %}}<li>Relire, c'est relou ?</li>{{% /fragment %}}

---

### Une étape à part entière 

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>🙏 Relisez vous, s'il vous plait !!</li>{{% /fragment %}}
{{% fragment %}}<li>Mal perçu en milieu professionnel 🤬</li>{{% /fragment %}}
{{% fragment %}}<li>Prévenez si vous avez des faiblesses en orthographe 🤒</li>{{% /fragment %}}
{{% fragment %}}<li>Il vaut mieux prévenir que d'être mal jugé</li>{{% /fragment %}}
{{% fragment %}}<li>Demandez à être relus 🧐</li>{{% /fragment %}}
</ul>

---

## Points à vérifier 1 par 1

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>✅ les pluriels des noms et des adjectifs</li>{{% /fragment %}}
{{% fragment %}}<li>✅ les accords des verbes</li>{{% /fragment %}}
{{% fragment %}}<li>✅ les participes passés</li>{{% /fragment %}}
{{% fragment %}}<li>✅ posez-vous la question du verbe être ou avoir aussi souvent que nécessaire</li>{{% /fragment %}}
</ul>

---

## Utiliser des outils

- Correcteur orthographique
- Outils en ligne
- Dictionnaire
- Bescherelle

---

### 🔧 Jeu d'essai d'une fonctionnalité

<ul>
<!-- {{% fragment %}}<li></li>{{% /fragment %}} -->
{{% fragment %}}<li>🛡️ But n°1 : parcourir tous les cas d’un code</li>{{% /fragment %}}
{{% fragment %}}<li>1 jeu de test = plusieurs cas 🎛️</li>{{% /fragment %}}
{{% fragment %}}<li>Quand ça marche 😀</li>{{% /fragment %}}
{{% fragment %}}<li>Quand ça ne marche pas, mais que c'est prévu 🤕</li>{{% /fragment %}}
{{% fragment %}}<li>Exemple : saisie OK et KO dans un formulaire</li>{{% /fragment %}}
</ul>

---

# Merci 

{{% fragment %}}
<div class="tenor-gif-embed center-img" data-postid="5435525" data-share-method="host" data-width="30%" data-aspect-ratio="1.0">
    <a href="https://tenor.com/view/stay-strong-baby-gif-5435525">Stay Strong GIF</a> from <a href="https://tenor.com/search/staystrong-gifs">Staystrong GIFs</a>
</div>
<script type="text/javascript" async src="https://tenor.com/embed.js"></script>
{{% /fragment %}}
