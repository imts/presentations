+++
title = "L'oral de l'examen"
description = "Quelques conseils pour préparer son oral d'examen du titre professionnel"
outputs = ["Reveal"]
[logo]
src = "../logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

{{< slide transition="zoom" background-image="simon-rae-unsplash.jpg" >}}

### Préparer son oral d'examen

<br /><br />
<small><span style="color:#00f">Photo by [Simon Rae](https://unsplash.com/@simonrae) - Unsplash</span></small>

---

#### L'oral d'examen pour le titre professionnel

💡 Quelques conseils et informations 🔦

{{% fragment %}} {{< tenor url="https://tenor.com/view/community-annie-dance-annie-dance-gif-3302947" ratio="1.0" >}}{{% /fragment %}}

---

#### Informations pour la présentation

<ul>
  <li class="fragment">⏰ Durée 35 minutes </li>
  <li class="fragment">Dont 10 minutes de démonstration max ⏳</li>
  <li class="fragment">Choisir <b>UN</b> projet</li>
  <li class="fragment">Repartir du rapport de stage (ou du DSP) 💼</li>
  <li class="fragment">Garder la structure du rapport 📗</li>
  <li class="fragment">Faire un support (Powerpoint ou autre)</li>
  <li class="fragment">Utilisation du rétro-projecteur 💻</li>
</ul>


---

### 🎓 Canevas du référentiel

<ul>
  <li class="fragment">Présentation de l’entreprise et contexte du projet</li>
  <ul><li class="fragment">cahier des charges </li>
  <li class="fragment">Environnement humain et technique</li></ul>
  <li class="fragment">Conception et codage des composants front-end et back-end</li>
  <li class="fragment">Présentation des éléments principaux de l’application</li>
  <li class="fragment">Présentation du jeu d’essai d'une fonctionnalité</li>
  <ul>
    <li class="fragment">Données en entrée, données attendues, données obtenues</li>
    <li class="fragment">Analyse des écarts éventuels</li>
  </ul>
  <li class="fragment">Présentation d’une recherche à partir de site anglophone</li>
  <li class="fragment">Synthèse et conclusion</li>
  <ul>
    <li class="fragment">Satisfactions et difficultés rencontrées</li>
  </ul>
</ul>

---

### Proposition de plan 🥅

<ul>
  <li class="fragment">Introduction 💬</li>
  <li class="fragment">Contexte du stage</li>
  <li class="fragment">Réalisations : Description de vos activités pendant votre stage</li>
  <li class="fragment">Démonstration</li>
  <li class="fragment">Conclusion</li>
</ul>

---

### Introduction 💬

- **Se présenter** : qui on est, pourquoi on est là
- Avoir une attitude ouverte positive

---

### 🏣 Contexte du stage

- Expliquer votre choix de stage
- Présentation de l'entreprise
- Environnement humain et technique : lieu, votre rôle
- Expliquer le sujet du stage, les objectifs, vos tâches (cahier des charges) en allant à l’essentiel

---

### Réalisations 💼

<ul>
  <li class="fragment">Description de vos activités pendant votre stage</li>
  <li class="fragment">Conception et codage : reprendre les étapes du rapport</li>
  <li class="fragment">Présentation des éléments de l’interface de l’application</li>
  <li class="fragment">Présentation du jeu d’essai d'une fonctionnalité</li>
  <ul>
    <li class="fragment">Données en entrée, données attendues, données obtenues</li>
    <li class="fragment">Analyse des écarts éventuels</li>
  </ul>
  <li class="fragment">Présentation d’une recherche à partir de site anglophone</li>
  <li class="fragment">Indiquer les livrables fournis (site, doc, ...)</li>
</ul>
 
---

### Démonstration 🔥

<ul>
  <li class="fragment">🌏 Aperçu global du site ou de l'application</li>
  <li class="fragment">🤿 Donner un exemple d'action utilisateur (inscription, traitements, achat, don, ...)</li>
  <li class="fragment">Montrer le parcours "qui marche" mais aussi ce qui est prévu quand ça ne marche pas (erreur de saisie) pour montrer la robustesse du site.</li>
</ul> 

---

### Conclusion 🎯

<ul>
  <li class="fragment">Synthèse du travail réalisé</li>
  <li class="fragment">Compétences développées</li>
  <li class="fragment">Apport du stage : bilan, quel impact sur votre vie professionnelle</li>
</ul>

---

### Quelques conseils

<ul>
  <li class="fragment">1 slide ~= 1 minute</li>
  <li class="fragment">Prévoir entre 25 et 30 slides (indicateur à ajuster en répétant).</li>
  <li class="fragment">Rappeler le sommaire pour articuler la présentation et voir l'avancement</li>
  <li class="fragment">Gérer son temps ⏳</li>
  <ul><li class="fragment">Avoir un timer, utiliser le mode présentateur ou une montre ⏰</li>
  <li class="fragment"><b>Répéter</b> en vous chronométrant ⏲️ pour savoir le temps pur chaque partie, pour avoir un repère</li>
  <li class="fragment">Préparer la démonstration à l'avance (ouvrez les fenêtres avant)</li>
  </ul>
  <li class="fragment">Parler à un rythme normal 🐢: Ce n'est pas une course</li>
</ul>

---

## Merci 

{{% fragment %}} {{< tenor url="https://tenor.com/view/etenee-presentation-gif-13299862" ratio="1.77" >}} {{% /fragment %}}


<br /><small>Retour à l'[accueil](https://imts.gitlab.io/presentations/) des présentations</small>
