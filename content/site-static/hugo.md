+++
title = "Hugo : générer vos sites statiques"
description = "Utiliser un générateur de sites statique - Hugo"
outputs = ["Reveal"]
[logo]
src = "../logo.svg"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

### À la découverte de Hugo

![Logo de Hugo](hugo-logo-wide.svg)
<small>Site web de <a href="https://gohugo.io">Hugo</a></small>

---

### Installation

<ul>
    <li class="fragment">L'<a href="https://gohugo.io/getting-started/installing">installation</a> est à votre charge !</li>
    <li class="fragment">Pour vérifier si ça fonctionne<br/>
        → dans une console</li>
</ul>

{{% fragment %}}
```bash
hugo
```
{{% /fragment %}}

<small class="fragment">Assistance possible sur Discord</small>

---

### Démarrage

{{% fragment %}}
###### Créer un nouveau site
- Dans un terminal ou avec `git bash`
```bash
hugo new site mon-site
cd mon-site
```
{{% /fragment %}}

{{% fragment %}}
###### Que se passe-t'il ?

- Création de dossiers 😊
- Tout est vide 😟
{{% /fragment %}}

---

#### Structure des dossiers

<!-- <img src="dossiers-new-site.png" alt="Dossiers à la création" class="right"> -->

![Dossiers new site hugo](dossiers-new-site.png#right)

- archetypes {{<frag c=": modèles de contenus">}}
- content {{<frag c=": le contenu, fichiers .md">}}
- data {{<frag c=": pour plus tard">}}
- layouts {{<frag c=": vos templates de mise en page">}}
- static {{<frag c=": ressources / assets css,js,images, ...">}}
- themes {{<frag c=": les thèmes !">}}
- **config.toml** {{<frag c=": fichier de configuration">}}

---

#### Fichiers de configuration

- Les **paramètres** du site
- Rangement en **sections**
- 3 formats possibles : **TOML**, **YAML**, JSON

<p class="fragment bleu">La configuration est aussi présente dans l'entête des Markdown <br> → format toml, yaml</p>
<h5 class="fragment rose">C'est le frontmatter</h5>

---

#### Format 1 : toml

- **``Clef = Valeur``**
- Texte entre `""`, Tableau entre `[]`, Nombres, booléens acceptés
- Sections
  - 1 ligne seule `[nom_section]`
  - Accès aux paramètres avec la notation pointée : nom_section.parametre


```toml
+++
date = "2016-12-14T21:27:05.454Z"
title = "TOML = Tom’s Obvious, Minimal Language"
tags = ["toml","yaml","json", "front matter"]

[sitemap]
  priority = 0.5
  filename = "sitemap.xml"
+++
```

{{% fragment class="bleu" %}}
→ Dans les fichiers .md, **frontmatter** délimitée par ``+++``
{{% /fragment %}}

---

#### Format 2 : yaml

- **``Clef: Valeur``**
- Texte par défaut ou entre `''`, Tableau = liste `-`, 
- Nombres, booléens acceptés
- Sections : clef sans valeur `nom_section:` + indentation

```yaml
---
date: '2016-12-14T21:27:05.454Z'
title: YAML Ain’t Markup Language
tags:
  - toml
  -yaml
  -json

sitemap:
  priority: 0.5
  filename: "sitemap.xml"
---
```

{{% fragment class="vert" %}}
→ Dans les fichiers .md, **frontmatter** délimitée par ``---``
{{% /fragment %}}

---

#### Format 3 : JSON

- Objet/structure délimité par `{}` 
- **``"Clef": "Valeur"``**
- Texte par défaut ou entre `''`, Tableau par `[]`
- Nombres, booléens acceptés
- Sections = sous-objets

```json
{
    "date" : "2016-12-14T21:27:05.454Z",
    "title" : "JSON : JavaScript Object Notation",
    "tags" : ["toml","yaml","json", "front matter"],

    "sitemap" : {
        "priority" : 0.5,
        "filename" : "sitemap.xml"
    }
}
```

{{% fragment class="rose" %}}
→ Dans les fichiers .md, **frontmatter** délimitée par ``{}``
{{% /fragment %}}

---

#### L'entête ou *Frontmatter*

<ul>
    <li class="fragment">Définit les propriétés du contenu</li>
    <li class="fragment"><strong>Paramètres</strong> accessibles dans les templates</li>
    <li class="fragment">Une partie <strong>générique</strong></li>
    <li class="fragment">Une partie très <strong>spécifique</strong> au thème</li>
</ul>

<h5 class="fragment rose">→ L'entête de configuration est un concept clef</h5>

---

#### Utiliser un thème 1/3 : installation

{{% fragment %}}
###### Choisir un [thème](https://themes.gohugo.io/)
{{% /fragment %}}

{{% fragment %}}
###### Nous allons utiliser *[ananke](https://themes.gohugo.io/gohugo-theme-ananke/)*

- Un bon exemple pour démarrer, très fonctionnel
- Blog (Articles), pages About et Contact
- Formulaire de contact
- ...

{{% /fragment %}}

{{% fragment %}}

```bash
cd themes
git clone https://github.com/budparr/gohugo-theme-ananke.git ananke
```

ou (si le site est en suivi de version)

```bash
git submodule add https://github.com/budparr/gohugo-theme-ananke.git themes/ananke
```

{{% /fragment %}}

---

#### Utiliser un thème 2/3 : configuration

<h6 class="fragment bleu">1. Recopier le dossier "<em>exampleSite</em>"</h6>

{{<frag c="Écraser les fichiers existants 👊">}}

<h6 class="fragment vert">📝 Éditer le fichier config.toml</h6>

{{% fragment %}}
Des noms de paramètres explicites 🤔

```toml
baseURL = "/"
languageCode = "fr-fr"
theme = "ananke"
themesDir = "themes"
```
{{% /fragment %}}

<h6 class="fragment rose">🚀 Tester le site (terminal VSCode)</h6>

{{% fragment %}}
```bash
hugo server -D
```
→ l'option ``-D`` affiche les brouillons (preview)
{{% /fragment %}}

---

#### Utiliser un thème 3/3 : du contenu ❕

<h6 class="fragment bleu">Dans le dossier "<b>content</b>"</h6>

<ul>
    <li class="fragment">Un sous-dossier par page</li>
    <li class="fragment">Les fichiers existants sont là pour vous aider 🤗</li>
    <li class="fragment">Observez les <b>Frontmatter</b></li>
    <li class="fragment">Fichier par défaut '<em>_index.md</em> (→index.html)'</li>
</ul>

<div class="fragment">
<h6 class="vert">Créer un nouvel article</h6>

```bash
hugo new posts/premier-article.md
```

<p class="fragment">Renommer <a href="https://gohugo.io/content-management/archetypes/" target="new">archetypes</a> / default.md → _default.md</p>

---

#### Utiliser les shortcodes

- [Shortcodes](https://gohugo.io/content-management/shortcodes/) : Raccourcis pour créer du contenu
- Les shortcodes de Hugo : figure, youtube, insta, tweet, ...
- Des shrotcodes pour chaque thème, dans ``themes/anake/layouts/shortcodes``
- Codés en Go : un nouveau langage à apprendre

---

### Quelques retours sur Hugo

<ul>
    <li class="fragment">Tout n'est pas intuitif dans Hugo</li>
    <li class="fragment">Analysez chaque page du site d'example en détail ...</li>
    <li class="fragment">L'idéal est de lire la doc du thème, souvent contenue dans les exemples ou le Readme du dépot</li>
    <li class="fragment">Lire la doc de Hugo est aussi nécessaire pour aller plus loin</li>
</ul>

<h6 class="fragment bleu">Cela prend du temps de s'y retrouver au début 🤯<br>
Inspirez / expirez 🤭</h6>

---

#### Exercice 1 : Formulaire de contact

- Créer un compte sur [Formspree.io](https://formspree.io/)
- Remplacer le lien dans la page contact.md
- Tester ...

<h6 class="fragment rose">🤩 Vous avez utilisé votre première API du Jamstack</h6>

---

#### Exercice 2 : mise en ligne sur les pages gitlab

- Côté Gitlab : créer un nouveau projet vide
- Côté Local :
  - Ajouter un fichier .gitignore contenant ``public``
  - Suivi les infos de Gitlab
- Dans le fichier config.toml :
    ```toml
    baseURL = "https://moi.gitlab.io/mon-depot/"
    ```
- Ajouter le modèle de CI de Hugo

<div class="fragment warning">

🧯 Si ça ne *fonctionne* pas, essayez :  
dans config.toml : ``baseURL = "/"``  
et dans .gitlab-ci.yml :

```yml
  script:
    - hugo -b https://moi.gitlab.io/mon-depot/ --minify
```

</div>

---

#### Exercice 3 : un nouveau site

- Recommencer un nouveau site Hugo
- Choisir / télécharger un thème (git)
- Conseils :
  - Un site par thème (pas très interchangeables)
  - [liva](https://themes.gohugo.io/liva-hugo/) : un blog à la Médium export en AMP !
  - [learn](https://themes.gohugo.io/hugo-theme-learn/) : thème utilisé pour les cours
  - [e-commerce](https://themes.gohugo.io/tags/ecommerce/) : un thème pour le e-commerce

<h6 class="fragment bleu">Il peut arriver qu'un thème ne soit pas facile à prendre en main ou présente des bugs ... essayez-en un autre</h6>

---

## Merci

<h5 class="vert">Vous êtes héroïques</h5>

{{% fragment %}}
{{< tenor url="3576538" ratio="1.145" >}}
{{% /fragment %}}
