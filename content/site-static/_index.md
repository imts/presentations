+++
title = "Les sites statiques"
description = "Utiliser un générateur de sites statique - Hugo"
outputs = ["Reveal"]
[logo]
src = "../logo.svg"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

## Les sites statiques

{{% fragment %}}
##### Mieux que l'UX, le DX: Développeur eXpérience
{{% /fragment %}}

{{% fragment %}}
{{< tenor url="5719019" ratio="1.09" width="30%" >}}
{{% /fragment %}}

---

### Principe et avantages

<ul>
  <li class="fragment">Des sites en HTML / CSS / JS</li>
  <li class="fragment">Rapidité : pas de back-end</li>
  <li class="fragment">Simplicité d'hébergement</li>
  <li class="fragment">Sécurité</li>
  <li class="fragment">Automatisation des tâches : des pages générées</li>
</ul>

<h6 class="fragment" style="color:#046db4;">→ Les pages sont générées 1 seule fois et publiées 😎 </h6>
<h6 class="fragment" style="color:#e7007c;">→ Plutôt qu'à chaque requête (PHP) 🤯</h6>

---

### Le [JAM](https://jamstack.org/)Stack 🤔

<h4 class="fragment"><span style="color:#046db4;; font-size:4rem;">J</span>avascript</h4>

{{<frag c="L'aspect dynamique du site est géré en JS côté client">}}

<h4 class="fragment"><span style="color:#8ebe1c; font-size:4rem;">A</span>PI</h4>

{{<frag c="Les briques logiques, gestion du contenu, BDD sur serveur sont séparées et accessibles via des API, interface de programmation d'application">}}

<h4 class="fragment"><span style="color:#e7007c; font-size:4rem; margin-bottom:8px;">M</span>arkup</h4>

{{<frag c="Les pages HTML sont générées && déployées">}}

<h5 class="fragment">→ Ce n'est pas une techno <br> plutôt un manifeste <b>Front-end</b></h5>

---

### Principe 🤿

<ul>
  <li class="fragment">Codage d'une page HTML/CSS/JS 😏</li>
  <li class="fragment">Transformation en template 🧐</li>
  <li class="fragment">📜 Le contenu dans format simple (e.g. Mardown)</li>
  <li class="fragment">Du dynamisme 🤸‍♂️ via les API</li>
  <li class="fragment">Des outils pour générer le site : <a href="https://www.staticgen.com/">Static site generators 🛠</a></li>
  <li class="fragment" style="color:#046db4;">→ Mise en ligne simple : `git push`</li>
</ul>

---

### Quelques API

- [Formspree](https://formspree.io/) : récupération des formulaires
- [Disqus](https://disqus.com/) : ajouter des commentaires
- [SnipCart](https://snipcart.com/fr) : un panier d'achat
- [Algolia](https://www.algolia.com/) : Moteur de recherche
- [Auth0](https://auth0.com/) : service d'autentification
- [Cloudinary](https://cloudinary.com/) : upload d'images et vidéos

<br />

<h4 class="fragment" style="color:#046db4;">→ Et pleins d'autres <a href="https://github.com/agarrharr/awesome-static-website-services">services</a></h4>

---

### Quelques inconvénients ?

- Un contenu moins personnalisé
- Modifier le contenu → regénérer le site
- Utilisation d'API tierces
---

### Autant d'opportunités

- Le site est construit automatiquement {{<frag c="→ Pas d'erreur" >}}
- Automatiser l'ajout de contenu {{<frag c="→ tâches planifiées" >}}
- Travail avec GIT (CI/CD) :
  - Toutes les modifs sont tracées
  - Un CMS naturellement collaboratif

---

### Les générateur de sites statiques

- Des outils pour générer le code
- Focus sur le travail <b>Front-End</b>
  - Template HTML/CSS
  - Fonctionnalités JS
- Des dizaines de générateurs {{% fragment %}} → [staticgen.com](https://www.staticgen.com/) {{% /fragment %}}

<br /><h5 class="fragment">Tour d'horizon des <a href="https://www.staticgen.com/">générateurs</a></h5>

---

# Let's go [Hugo](https://gohugo.io) 💪

{{% fragment %}}
{{< tenor url="5719019" ratio="1.09" >}}
{{% /fragment %}}
