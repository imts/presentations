+++
title = "Institut Solacroup"
description = "Écran d'accueil"
outputs = ["Reveal"]
[logo]
src = "logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
# Régler auto_slide pour un diaporama automatique
# valeur en millisecondes, 0 pour défilement manuel
auto_slide = 2000
loop = true
transition = "slide"
transition_speed = "fast"
+++

{{< slide transition="zoom" background-image="banner.jpg" >}}

### <span style="color:#fff">Bienvenue à l'Institut Solacroup</span>

<small style="color:#fff">Quelques présentations et autres infos</small>

---

## 🔥 Supports de présentations 1/2

- 😁 [Présentation à l'oral](/examen-oral/), préparation de l'examen développeur
- 🎳 [Tester vos sites](/test-pages/) et pages web
- 📝 [Écrire son rapport de stage](/rapports/) : des conseils
- 🗂 [Retour sur les DP](/retour-dsp/) : retour sur les dossier professionnels
- 🗿 [Créer une présentation web](/presentations/) avec Hugo et Reveal JS
- 📟 [Migrer vos sites locaux](/local-web/) chez un hébergeur

---

## ⚡ Supports de présentations 2/2

- ✨[Les Sites statiques](/site-static/)
- 🎲[OpenSCAD](/openscad/) : la modélisation 3D par programmation

---

## 1ère Session d'examen 3/4 juin 🎓

### Pour l'obtention du titre de développeur Web / Web Mobile

- Réussite :
  - Thomas Mathécade
  - Quentin Loustau
  - Kassandra Aubin
- Échec :
  - Alexandre Touchet

###### → 2è session : 8/9/10 septembre 2020

---

# 📣 Infos importantes

- 😷 Le déconfinement est commencé 😿
- Les corsaires peuvent revenir à l'institut pour travailler sur inscription
- Fin de la formation 30 juin

---

# 🍴 🍕 Menu 🍖 🍪

- Le riz chorizo vous manque 🍚 ?
- Steack Tartare + 🍟
- 🍩 un donut 3D par Kévin

---

{{< slide background-image="1234.jpg" >}}

---

##### La journée portes ouvertes ~~4 avril~~ est annulée 😱

{{< figure src="Flyer Portes Ouvertes.png" width="85%">}}

