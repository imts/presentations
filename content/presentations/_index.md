+++
title = "RevealJS et Hugo pour les présentations"
description = "Faites des présentations web avec Hugo et RevealJS"
outputs = ["Reveal"]
[logo]
src = "logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

### Faites des présentations web avec Hugo et RevealJS

{{< figure src="dead-powerpoint.jpg" width=400 title="Powerpoint is dead !" >}}

---

## 🧭 Les étapes

##### 🔧 Installation
##### 🔨 Configuration 
##### {{< emoji ":metal:" >}} Utilisation

---

## 🔧 Installation

- 🔥 Prérequis : le gestionnaire de paquets 
  - {{< emoji ":chocolate_bar:" >}} Chocolatey pour Windows
  - 🍎 Homebrew pour MacOs
  - 😎 apt-get pour Linux
  - :package: [Git](https://git-scm.com/download/) : lors de l'installation, cocher 

- Installer Hugo : site https://gohugo.io/getting-started/
  - 📋 Une ligne à recopier
  
---

## 🔧 Tester l'installation Installation

Ouvrez un terminal et tapez : 

```bash
hugo
```

--- 

## 🔨 Configuration 

---

## :metal: Utilisation

---

## 💅🏼 Customiser le thème :lipstick: