+++
title = "OpenSCAD - programmer la 3D"
description = "Présentation du logiciel OpenSCAD, un outil idéal pour programmer des pièces techniques en 3D"
outputs = ["Reveal"]
[logo]
src = "../logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

### OpenSCAD - programmer la 3D

{{< figure src="openscad.png" title="" >}}

---

### Présentation 🤩

<ul>
  <li class="fragment">Un logiciel de modélisation 3D</li>
  <li class="fragment">Que du code : Idéal pour les pièces techniques</li>
  <li class="fragment">Des pièces en <strong>Open Source</strong></li>  
  <li class="fragment">Le Customizer : des pièces paramétriques 💣</li>
  <li class="fragment">Pas mal de librairies <span class="fragment">→ <a href="https://www.thingiverse.com">Thingiverse</a></span></li>
</ul>

---

### Quelques exemples

---

### Démarrage rapide

#### Installation / lancement

Logiciel à télécharger sur [openscad.org](http://www.openscad.org/) 

#### Lancement 🚀

Lancer le logiciel

```cpp
difference() {
  cube(15, center=true);
  sphere(r=10);
}
```

<h6 class="fragment">F5 pour le rendu, F6 pour générer le STL</h6>
<h6 class="fragment">Voir les messages dans la console</h6>

---

#### La documentation 📚

- 📃 [le Cheat Sheet en 1 page](http://www.openscad.org/cheatsheet/index.html) : Indispensable 🧐
- Documentation complète en ligne 📰

---

#### Des exemples

- Parcourir les exemples pour bien commencer 🧐
- Chercher l'inspiration sur [Thingiverse:'Openscad'](https://www.thingiverse.com/search?q=openscad&type=things&sort=relevant)


---

### C'est du code !

{{% fragment %}}

###### Tout pour programmer

- Variables
- Boucles
- Condition
- Fonctions (modules)

{{% /fragment %}}

{{% fragment %}}

###### Pour Déboguer

Devant une ligne :

- `#` : objet en rouge (mise en avant)
- `%` : objet en transparent (répérage)
- `!` : mode solo
- `*` : enlève l'objet

{{% /fragment %}}

---

### 🎁 Bonus 1 : le Customizer


---

### 🎁 Bonus 2 : la librairie [MCAD](https://github.com/openscad/MCAD)

- Des pièces mécaniques à inclure en 1 ligne

---

### 🎁 Ressources

- 👏 Un [tuto par le Fablab de Lannion](https://static.fablab-lannion.org/tutos/openscad/)
- 😎 [Openjscad](http://openjscad.org/) : Openscad en ligne !
- 🤯 [Faire des animations](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Example/Strandbeest) : avancé
