+++
title = "Tester vos pages Web !"
description = "Un guide de bonnes pratiques pour tester vos pages web."
outputs = ["Reveal"]
[logo]
src = "../logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

### Tests et audit d'un site web

<img src="tester.png" alt="Testez vos sites !" width="30%">

<small>Voir les <a href="https://imts.gitlab.io/cours-imts/02.web/html/tester-une-page/">notes de cours</a> associées</small>

---

## 🔧 Les fondamentaux 🔎

##### {{< frag c="🎳 Tester" >}}
##### {{< frag c="🧐 Tester" >}} 
##### {{< frag c="💉 Et encore tester !" >}}

##### {{< frag c="⌛️⌛️⌛️ C'est un long process ⌛️⌛️⌛️" >}}

--- 

### Les étapes 👣

<ol>
    <li class="fragment">Vérifier que le site ne contient <b>pas d'erreur</b> 👌</li>
    <li class="fragment">Le site est <b>utilisable</b>
    <ul>
      <li>Le contenu s'affiche correctement</li>
      <li>Les utilisateurs arrivent facilement à leur but 🤙</li>
    </ul></li>
    <li class="fragment">Vérifier que le site est <b>rapide</b> 🚀</li>
</ol>

---
{{% section %}}

### 1. Valider votre code 🚀

- Vérifiez l'intégrité de votre code HTML
- Validité du DOM
- 1 balise ouverte doit être fermée ...
- Éviter les liens cassés
 
---

#### La structure DOM HTML

📋 Vérifier dans Firefox le code source de la page :

- ``Ctrl+u`` ou ``clic droit → code source de la page``
- Les erreurs de syntaxe, balises non fermées, ...,  apparaissent en **<span style="color:#f00">rouge</span>**. 

---

📋 Faire une vérification syntaxique de votre code HTML 

Utiliser le site [validator.w3.org](https://validator.w3.org/) → corriger toutes les erreurs que vous indique le parser syntaxique. 🧐

---

📋 Vous pouvez aussi faire d'autres validations sur le site ou avec d'autres parsers

Par exemple [Structured Data linter](http://linter.structured-data.org/)

--- 

#### Valider les liens 🔗

- Pas de liens cassés 🏚
- Utiliser le [w3 validator checklink](https://validator.w3.org/checklink)
- Outil dédié : [Xenu's link sleuth](http://home.snafu.de/tilman/xenulink.html)

<small>[Guide sur SeoMix](https://www.seomix.fr/xenu/)</small>

{{% / section %}}

---

{{% section %}}

### 🧐 2. Valider le rendu visuel 🎨

#### Valider le CSS 📱💻

- Vérifier l'aspect **responsive** du site

<br />
<h5 class="fragment"> → Utiliser l'inspecteur du navigateur</h5>

--- 

#### Tester les différents navigateurs 

###### Mobile First 🤳

- Android (Chrome, Firefox, Opera) 
- iOS (Safari)

<br /><h5 class="fragment"> → Demander l'aide d'un ami</h5>

--- 

#### Tester les différents navigateurs 

###### Sur ordinateur

- Windows (Edge, Internet Explorer, Chrome, Firefox, Opéra, ...)
- Mac OS : Safari
- Linux : Chrome, Firefox

---

#### Les outils 

Génération de capture d'écran

- http://browsershots.org/ : tests Linux et Mac
- https://crossbrowsertesting.com/ (payant)

{{% /section %}}


--- 

{{% section %}}

#### 3. Dernière passe sur Javascript

- Le code doit être débogué
- La **console** Javascript est votre amie 📟
- Pour la validation, filtrer **erreurs** et **avertissements**

![imagesconsole-warn-err.png](console-warn-err.png)

--- 

#### Vérifier le code Javascript

- 🔥 il n'est pas toujours possible de corriger les warnings ou les erreurs suivants les librairies utilisées.

<br />
<h5 class="fragment"> → Dans ce cas, il faut bien s'assurer que le site reste fonctionnel</h5>

{{% / section %}}

---

{{% section %}}

### Speed Matters !

🔥 Tests sur le taux de conversion des sites 

<img src="speed-matters-googleio19.png" alt="speed-matters-googleio19" width="80%">

---

### La rapidité en priorité

<ol>
  <li class="fragment">la vitesse est 3 x plus importante que l'aspect visuel 📈</li>
  <li class="fragment">Utiliser des visuels standards (bootstrap, cards, materialize)</li>
</ol>

{{% /section %}}

---
{{% section %}}

### 🎓 L'audit 

#### Lighthouse et les autres 

<ul>
  <li class="fragment">À ce stade, votre site est fonctionnel</li>
  <li class="fragment">L'audit donne des conseils pour l'améliorer</li>
</ul>

--- 

#### Lighthouse

![Extension Lighthouse](lighthouse-icon.png)

##### L'outil numéro 1

<ul>
  <li class="fragment"> Pour faire les tests</li>
  <li class="fragment">Pour s'améliorer</li>
</ul>

<small class="fragment">📣 Utiliser Google Chrome pour lancer l'audit, dans l'onglet [Audits] des DevTools</small>

<small class="fragment">Les critères analysés sont expliqués en détail [ici](https://developers.google.com/web/tools/lighthouse/v3/scoring)</small>

---

### Revue des critères

![Exemple de résultat d'audit Lighthouse](ex-lighthouse-report.png)

→ Voir le guide d'utilisation de [Lighthouse](https://developers.google.com/web/tools/lighthouse/)

---

#### #1 **Performances** 

Temps de chargement de votre page ainsi que l'utilisation du CPU qui peut être problématique avec Javascript.

  - score > 90 : site rapide 😎
  - score > 50 : site moyen 😕
  - score <50 : site lent 😰

---

#### #2 Accessibilité

- Obligatoire pour les sites web et applications mobiles des organismes publics
- Si besoin voir les guides :
  - [Vue d’ensemble des standards d’accessibilité du W3C](https://www.w3.org/WAI/standards-guidelines/fr)
  - [Principes d’accessibilité](https://www.w3.org/WAI/fundamentals/accessibility-principles/fr)

--- 

#### #3 Bonnes pratiques

> Les bonnes pratiques web sont en constantes évolutions.  
> Les points audités par ce critère sont donc intéressant à suivre. 


<h5 class="fragment">→ Voir la page <a href="https://web.dev/vitals/">Web Vitals</a></h5>

---

#### #4 Référencement : SEO

Des critères assez simples à mettre en place

- Il faut viser les 100%
- Ce n'est pas une stratégie SEO mais bien pour un bon référencement naturel

---

#### #5 Web App : PWA

- Analyse des critères pour faire de votre site une PWA


{{% /section %}}

---

### Recommandations pour l'utilisation de LightHouse 


<ul>
  <li class="fragment">💡 Il est important de lire l'ensemble des conseils fournis</li>
  <li class="fragment">Tous ne pourront pas être appliqués</li>
  <li class="fragment">Vous ne les comprendrez pas tous</li>
</ul>

{{% fragment %}}

> Ils vous permettrons tous de progresser ❣  
> 📆 Creuser un nouvel aspect à chaque audit de site

{{% /fragment %}}

--- 


##### Remarques sur les images

Lighthouse suggère d'encoder les images au format WebP. 

Le problème est que ce format [n'est pas supporté](https://caniuse.com/#search=webp) pas tous les navigateurs (notamment Safari et IE). 

{{% fragment %}}

Pour l'utiliser, il faut donc encore proposer l'alternative en PNG/JPG

```html
<picture>
	  <source srcset="/cours-imts/images/logo.webp" type="image/webp">
	  <img src="/cours-imts/images/logo.png" id="logo" alt="IT Nota Logo example">
</picture>
```

{{% /fragment %}}

{{% fragment %}}

🛠 L'outil [cwebp](https://developers.google.com/speed/webp/) permet de convertir les images en ligne de commande.

{{% /fragment %}}

---

#### Politique de cache


<ul>
  <li class="fragment">Notion importante (explications spécifiques)</li>
  <li class="fragment">Des explications très claires sur <a href="https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching#defining-optimal-cache-control-policy">developers.google.com</a></li>
  <li class="fragment">📌 Ensemble de guides sur les fondamentaux du web</li>
</ul>

---

#### L'audit de rapidité : PageSpeed

L'outil [PageSpeed Insight](https://developers.google.com/speed/pagespeed/insights/) permet de tester plus spécifiquement la rapidité d'une page. 

---

{{% section %}}

### D'autres ressources 

🔖 Un article intéressant sur [la Fabrique du Net ](https://www.lafabriquedunet.fr/blog/audit-site-web-fondamentaux/) présentant les fondamentaux de l'audit d'une page web sous différents aspects, notamment le e-commerce. 

---

#### outils

- https://www.dareboost.com/ : analyse complète, infos détaillées en français (5 analyses gratuites / mois)
- https://www.webpagetest.org/ : permet de tester la mise en cache (la page est rechargée 3 fois)
- https://tools.pingdom.com/ 

{{% /section %}}

---

{{% section %}}

## Alléger vos pages

Le **gros** du téléchargement d'une page web provient :

- Des **images 📷** 
- Des **vidéos 🎞**
- De certains scripts : utiliser le minify

<h5 class="fragment">Chaque kilo compte !</h5>

---

#### Traiter les images en 2 étapes

<ol>
  <li class="fragment">Redimensionner les images au plus juste : utiliser des vignettes</li>
  <li class="fragment">utiliser 2 images de tailles différentes en fonction de la taille de l'écran</li>
</ol>

<h6 class="fragment">En css avec les <b></b>media queries</b></h6>

<pre class="fragment"><code class="language-html">
<link rel="stylesheet" media="screen and (min-width: 900px)" href="widescreen.css">
<link rel="stylesheet" media="screen and (max-width: 600px)" href="smallscreen.css">
</code></pre>

---

###### Sauver les photos en JPEG avec un taux de compression adapté

  - 80% pour des vignettes
  - 85% pour des images
  - Inutile d'aller au-delà de 90% de compression (traitement, agrandissement, ...

--- 

###### Sauver les diagrammes en PNG 

- Bon format pour les dessins sur ordinateur 
- Utiliser le mode indexé (voir Astuce vidéo)

---

###### Utiliser SVG pour les logos

- Format adapté aux dessins vectoriels (Inkscape, Illustrator)
- Optimal en taille
- Fichier externe ou texte

---

**📲 Mobile First 🥇** 

**→ La tendance actuelle est de faire le design d'abord pour le téléphone !**

- Connaître les résolutions standard 
  - téléphone
  - tablettes
  - PC, Mac
- Un bon tuto de w3schools sur les [media query](https://www.w3schools.com/css/css_rwd_mediaqueries.asp).

{{% /section %}}

---

{{% section %}}

### Faire des tests utilisateurs 

<ul>
  <li class="fragment">Votre premier utilisateur sera sans doute votre client</li>
  <li class="fragment">Il n'est pas forcément lui-même son client et peut avoir un avis subjectif biaisé</li>
  <li class="fragment">Ne pas focaliser trop sur l'esthétique la couleur des boutons, les logos, ... </li>
</ul>

<br />
<h5 class="fragment">Il est donc important de faire des tests avec de vrais utilisateurs cibles</h5>

---

### Faire des tests utilisateurs 

- À défaut, faites tester le site à vos amis, aux autres corsaires. 
- Donner leur une tâche précise à accomplir sur le site, un **scénario utilisateur**, pour aller au-delà d'une navigation juste esthétique. 
- Valider l'efficacité du **Call To Action**

{{% /section %}}

---

### 📖 L'orthographe : c'est vous qu'on juge !

> Quand vous faites une faute, on ne juge pas votre orthographe, mais on Vous juge 💘

→ 💡 Reliser vous, Faites vous **relire**

---

## Synthèse

<ul>
  <li class="fragment">Un code valide : pas d'erreur (HTML / CSS / JS)</li>
  <li class="fragment">Des pages responsives : mobile first</li>
  <li class="fragment">Un chargement rapide = des tests longs</li>
  <li class="fragment">Un contenu sans faute</li>
  <li class="fragment">Un design efficace</li>
</ul>

---

### C'est à vous 

{{% fragment %}} {{< tenor url="https://tenor.com/view/success-yes-complete-pass-gif-16966223" ratio="1.0" >}} {{% /fragment %}}


<br /><small>Retour à l'[accueil](https://imts.gitlab.io/presentations/) des présentations</small>
