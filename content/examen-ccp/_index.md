+++
title = "L'examen du titre professionnel"
description = "Les explications du déroulement de l'épreuve pour valider les CCP"
outputs = ["Reveal"]
[logo]
src = "../logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

#### Examen pour le titre professionnel

{{< figure src="hunters-race-MYbhN8KaaEc-unsplash.jpg" title="Développeur web et web mobile" width="50%">}}

<small><br />Photo by [Hunters Race](https://unsplash.com/@huntersrace) on [Unsplash](https://unsplash.com/s/photos/work)</small>

---

#### Les éléments envoyés au Jury


<ol>
  <li class="fragment">Dossier de projet ( = rapport de stage)</li>
  <li class="fragment">Dossier professionnel</li>
  
</ol>

<br />

{{% fragment %}}
##### → Envoi avant le 18 mai 🚀

Remise au jury 15 jours avant l'épreuve
{{% /fragment %}}


---

### Composition du jury

<ul>
  <li class="fragment">2 professionnels du développement web</li>
  <li class="fragment">2 membres de l'IMTS</li>
</ul>

---

![entretien-ccp-titre-1.png](entretien-ccp-titre-1.png)

<img class="fragment" width="35%" src="entretien-1.jpg">
<img class="fragment" width="40%" src="entretien-2.jpg">
<img class="fragment" width="15%" src="entretien-3.jpg">

---

###### Présentation du projet : 35 min

<img class="center-image" src="entretien-1.jpg">

<ul>
  <li class="fragment">Préparer un support de présentation</li>
  <li class="fragment">La présentation porte sur 1 projet (stage)</li>
  <li class="fragment">Elle inclut 10 min de démo <b>maximum</b> 🔥</li>
  <li class="fragment">Le projet présente les compétences principales</li>
</ul>


---

###### Compétences obligatoires CCP 1

{{% fragment %}}

- **Maquetter** une application
- **ET** au choix :
  - Réaliser la partie front **dynamique** et **responsive**
  - Réaliser la partie front avec un **CMS**

{{% /fragment %}}

<br />
<h6 class="fragment">Compétences obligatoires CCP 2</h6>

{{% fragment %}}

- Développer les **composants d’accès** aux données
- **ET** au choix : 
  - Développer la partie **back-end** d’une application web
  - Élaborer des **composants** dans un **CMS**

{{% /fragment %}}

---

###### Entretien : 40 min

<img class="center-image" src="entretien-2.jpg">

<ul>
  <li class="fragment">Valider les compétences non couvertes</li>
  <li class="fragment">Utiliser les autres projets</li>
  <li class="fragment">Présentation plus informelle (fiches synthétiques)</li>
  <li class="fragment">Limiter les démos</li>
  <li class="fragment">Prendre du recul sur le développement</li>
</ul>

---

###### Discussion / conclusion : 15 min

<img class="center-image" src="entretien-3.jpg"><br />

<ul>
  <li class="fragment">Temps d'échange</li>
  <li class="fragment">Projet professionnel</li>
  <li class="fragment">Poursuite de parcours</li>
</ul>


---

###### Préparation / conclusion pour le jury : 30 min

<ul>
  <li class="fragment"> 15 min. préparation / présentations</li>
  <li class="fragment">15 min. délibération</li>
</ul>

---

<img class="center-image" src="entretien-full-jury.jpg">

---

### Un mot sur l'épreuve 

<h5 class="fragment">Le jury est bienveillant<br /> il cherche à comprendre vos acquis</h5>

<ul>
  <li class="fragment">Le programme est <b>très</b> ambitieux</li>
  <li class="fragment">Avoir compris les notions abordées</li>
  <li class="fragment">Être conscient de ses forces / faiblesses</li>
  <li class="fragment">Ne pas se sous-estimer</li>
</ul>

---

## Merci 

{{% fragment %}} {{< tenor url="https://tenor.com/view/stay-strong-you-got-this-youre-the-boss-you-rock-beat-negativity-gif-15343987" ratio="1.33" >}} {{% /fragment %}}


<br /><small>Retour à l'[accueil](https://imts.gitlab.io/presentations/) des présentations</small>
