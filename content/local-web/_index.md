+++
title = "Sites locaux → Hébergeur"
description = "Les explications pour migrer vos sites en locaux chez un hébergeur"
outputs = ["Reveal"]
[logo]
src = "logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

### Migrer vos sites web chez un hébergeur

{{< figure src="local-hebergeur.png" title="La méthode en 3 étapes" >}}

---

{{< figure src="Migration-local-hebergeur-1.png" width="600" title="" >}}

---

{{< figure src="Migration-local-hebergeur-2.png" width="600" title="" >}}

---

#### Prise en main de l'hébergement

<ul>
  <li class="fragment">Transfert des fichiers : 🧪 Tester la connexion avec FileZilla</li>
  <ul><li class="fragment">🔎 Identifier le dossier racine du site
    <li class="fragment">`www`, `httpdoc`, `public_html`, ... ?</li></li></ul>
  <li class="fragment">BDD Faut-il la créer ? Ses utilisateurs ?
    <ul><li>Penser à bien définir les privilèges des utilisateurs 🔏</li></ul></li>
  <li class="fragment">Valider les identifiants de la base de données 🛡
  <ul><li>Utiliser un script de test 📝</li></ul></li>
</ul>

---

```php
<?php
// Identifiants
$servername = "localhost";
$dbname = "db";
$username = "user";
$password = "pass";

try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  echo "<h1>Connexion réussie !</h1>\n";
  $conn = null;
}
catch(PDOException $e)
{
  echo "<h1>Échec de la connexion</h1>\n";
  echo "Connection failed: " . $e->getMessage();
}
?> 
```

<br />

<h5 class="fragment">🚀 On y va ?</h6>

---

{{< slide transition="none" >}}

{{< figure src="Migration-local-hebergeur-step-0.png" width="1800" title="" >}}

---

{{< slide transition="none" >}}

{{< figure src="Migration-local-hebergeur-step-1.png" width="1800" title="" >}}

---

###### Export de la BDD

<ul>
  <li class="fragment">Création d'un fichier SQL avec PhpMyAdmin</li>
  <li class="fragment">C'est du texte : il est possible de l'éditer 📝</li>
  <li class="fragment">Conformité des liens : Rechercher / remplacer</li>
</ul>

<h6 class="fragment">🔥 Gestion des accents</h5>

<ul>
  <li class="fragment">Encodage `utf8_general_ci` ou `utf8mb4_unicode_ci`</li>
  <li class="fragment">Vérifier en local et sur l'hébergeur</li>
</ul>

{{% fragment %}}

```php
$conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
```
{{% /fragment %}}

---

### Étape #2 : copie des fichiers

---

{{< figure src="Migration-local-hebergeur-step-2.png" width="1800" title="" >}}

---

#### Upload des fichiers 🌐 ⤴️

<ol>
  <li class="fragment">Faites une copie en local du dossier 💾</li>
  <li class="fragment">Modifiez les infos de configuration 🔑 : connexion à la BDD, ...</li>
  <li class="fragment">Envoyer les fichiers avec FileZilla</li>
</ol>

---

### Étape #3 : import SQL 📥

--- 

{{< figure src="Migration-local-hebergeur-step-3.png" width="1800" title="" >}}

---

###### Import du fichier SQL 📥

<ul>
  <li class="fragment">Utiliser PhpMyAdmin côté hébergeur</li>
  <li class="fragment">🔧 Créer un script d'import si besoin</li>
  <li class="fragment">🔒 Protéger le script ou supprimer le !</li>
</ul>

{{% fragment %}}
💡 Utiliser des fichiers serveur pour gérer l'accès à un dossier :  
 `.htaccess` et `.htpasswd` 
{{% /fragment %}}

---

### Importation terminée

{{% fragment %}} 
Il faut maintenant ...
{{% /fragment %}} 

{{% fragment %}} 
##### ... Tester votre application 🤒
{{% /fragment %}} 
 


---

{{< figure src="Migration-local-hebergeur-step-3.png" width="1800" title="" >}}

---

### Exercice

- Créer un espace chez un hébergeur gratuit (Planethoster, ou Webhost)
- Récupérer les fichiers du TP 
- Vérifiez qu'ils fonctionnent en local
- Faites migration vers l'hébergeur

---

## Merci 

{{% fragment %}} {{< tenor url="https://tenor.com/view/stay-strong-baby-gif-5435525" >}} {{% /fragment %}}

<small>Retrouver la [documentation associée](https://imts.gitlab.io/cours-imts/02.web/wordpress/migration-wp/) à cette présentation</small>

<br /><small>Retour à l'[accueil](https://imts.gitlab.io/presentations/) des présentations</small>
