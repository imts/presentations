+++
title = "Retour sur les dossiers professionnels"
description = "Quelques conseils pour remplir le dossier professionnel"
outputs = ["Reveal"]
[logo]
src = "logo.png"
alt = "Institut Marie-Thérèse Solacroup"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
+++

<br />

#### 🗂 Retours sur les dossiers professionnels

<br />

<ul>
  <li class="fragment">Les objectifs 🔭</li>
  <li class="fragment">Le contenu 🩺</li>
  <li class="fragment">La forme 🔍</li>
</ul>
 
--- 

### Un document officiel et obligatoire 👮‍♂️

<ul>
  <li class="fragment">Présentez de vos expériences dans le domaine 👨🏾‍💻</li>
  <li class="fragment">Utile au jury pour mieux vous connaître 🧙‍♂️ </li>
  <li class="fragment">Préparation à l'entretien avec le jury, et à la prise de parole 💬 </li>
</ul>

{{% fragment %}} ###### 🛠 Un outil au délà de l'examen {{% /fragment %}} 
<ul>
  <li class="fragment">CV détaillé pour passer des entretiens</li>
</ul>

<br />

<small>Toutes les infos sur [dossierprofessionnel.fr](http://www.dossierprofessionnel.fr)</small>

--- 

### 🏗 Construction du dossier

<ul>
  <li class="fragment">📽 Une 1<sup>ère</sup> présentation des DP a été faite en décembre</li>
  <li class="fragment">👌🏽 Cette nouvelle version ne remet pas en cause ce qui est fait</li>
  <li class="fragment">🙈 Aviez vous fait les corrections suggérées  ⁉️ </li>
</ul>
 
<br />

{{% fragment %}} ##### 🌟 Pour la version finale{{% /fragment %}}

{{% fragment %}} ##### Ajoutez des exemples pour les compétences acquises 🧠 {{% /fragment %}}

--- 

### Présenter son travail 🖥

<ul>
  <li class="fragment">📽 Vous avez déjà fait le travail</li>
  <li class="fragment">Il faut <i>juste</i> <b>Reformuler</b></li>
  <li class="fragment">Prendre du recul et <b>synthétiser</b> les étapes</li>
  <br />
  <li class="fragment">💄 Le <i>Front-end</i> du dossier est important 👑</li>
  <li class="fragment">Une belle présentation, illustrée et agréable à lire</li>
</ul>

<br />

{{% fragment %}} ##### Il ne faut pas toucher à la structure 🙅‍♀️{{% /fragment %}}

--- 

### Contenu d'un exemple #1

{{% fragment %}} ###### 📣 1. titre explicite 
- Site / une page web / des fonctions javascript ou PHP.  
  Par exemple :
  
  <small>"Design Sprint : refonte du site les fourmis solidaires"</small><br />
  <small>"Réalisation d’un portfolio web dynamique et publication sur Gitlab"</small><br />
  <small>"Codage d’une galerie d’images en PHP"</small>
{{% /fragment %}}

{{% fragment %}} ###### 2. 🔎 Donner un aperçu global du projet

- Donner le but du projet : 1 phrase ou 2
- Contexte, objectifs, langage utilisé
- Détailler le titre !
{{% /fragment %}}

--- 

### Contenu d'un exemple #2

{{% fragment %}} ###### 3.  La liste des étapes 📑

- Pensez à relier les étapes et les compétences 🔩
  
  <small>"Maquettage du site et conception de la charte graphique"</small><br />
  <small>"Codage en HTML de la structure de la page"</small><br />
  <small>"Codage en CSS avec le Framework `xyz` de la mise en page"</small><br />
  <small>"Ajout du scrolling dynamique en Javascript"</small>
{{% /fragment %}}

{{% fragment %}} 

- 🎖 Décrivez comment vous avez réussi à faire chaque étape. 
{{% /fragment %}}

---

### Un bon exemple de dossier professionnel

Le [dossier de Maxime](https://imts.gitlab.io/cours-imts/03.documentation/redaction-dp.files/Exemple-Dossier-professionnel-Maxime-Lefort.pdf) est un bon exemple 🏋🏽‍♂️

---

### Rappels 

{{% fragment %}} ###### Écrire au présent

- Comme le rapport de stage
{{% /fragment %}}

{{% fragment %}} ###### Soignez l'orthographe

- L'orthographe, c'est important
- Relisez vous
- Réécoutez les conseils de rédaction de rapports pour plus d'infos sur l'orthographe
{{% /fragment %}}

--- 

### Titre d'un document électronique

<ul>
  <li class="fragment">Explicite et informatif</li>
  <li class="fragment"><b>Objet</b> du document</li>
  <li class="fragment"><b>Nom</b> de l'auteur</li>
  <li class="fragment"><b>Date</b> (yyyy-mm-jj) et un numéro de version pour trier</li>
</ul>

---

### Exemples de titre

{{% fragment %}} ###### À suivre {{% /fragment %}} 

<ul>
  <li class="fragment">"Rapport de Stage Socastek - Bill Todd - 2020-04-14.pdf"</li>
  <li class="fragment">"Dossier Professionnel - Jane Doe - 2020-04-09.pdf"</li>
</ul>

{{% fragment %}} ###### À éviter {{% /fragment %}}

<ul>
  <li class="fragment">"Rapport (4).pdf"</li>
  <li class="fragment">"Mon Rapport de stage.pdf"</li>
  <li class="fragment">"Dossier professionnel (trame).pdf"</li>
</ul>

--- 

Laissez <b>“trame”</b> ou <b>“modèle”</b> dans un nom de fichier, c’est un peu comme quand vous laissez l’étiquette d’un habit que vous venez d’acheter

{{% fragment %}} {{< tenor url="https://tenor.com/view/retail-price-tag-price-gif-9214998" title="L'étiquette" width="50%" ratio="2.25" >}} 
{{% /fragment %}}

---

## Merci 

{{% fragment %}} {{< tenor url="https://tenor.com/view/stay-strong-baby-gif-5435525" >}} {{% /fragment %}}

<small>Retrouver la [documentation associée](https://imts.gitlab.io/cours-imts/03.documentation/redaction-dp/) à cette présentation</small>
